export const priceText = (deal_type, catalog, price, size) => {
  if(deal_type !== 'Izīrē' && catalog) {
    return "€" + " (" + Math.round(price/size) + " €/m2)";
  }
  return "€/mēn.";
}