import React from 'react';
import Slider from 'react-slick';
import { Link } from 'react-router-dom';
import './styles.css';

const Item = ({ data }) => {
  const priceText = () => {
    if(data.deal_type !== 'Izīrē') {
      return "€" + " (" + Math.round(data.price.replace(/\s/g, "")/data.size) + " €/m2)";
    }
    return "€/mēn.";
  }

  return (
    <Link to={`/catalog/${data.id}`} className="item-container">
      <div className="images">
        <img src={data.images_thumb[0]} />
        {/* <Slider adaptiveHeight={true}>
          {data.images_thumb.map((item, i) => <div key={i} className="images-container"><img src={item} /></div>)}
        </Slider> */}
      </div>
      <div className="item-info-block">
        <div className="item-location">
          <p className="address">{data.city + ", " + data.iela}</p>
          <p className="district">{data.district}</p>
        </div>
        <div className="item-info">
          <div>
            <p>Istabas</p>
            <p>{data.rooms}</p>
          </div>
          <div>
            <p>Platība</p>
            <p>{data.size + "m2"}</p>
          </div>
          <div>
            <p>Stāvs</p>
            <p>{data.floor + "/" + data.floor_count}</p>
          </div>
        </div>
        <div className="item-price">
          <p>{data.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")}</p>
          <span>{priceText()}</span>
        </div>
      </div>
    </Link>
  )
}

export default Item;
