import React, { Component } from 'react';
import Slider from 'react-slick';
import { connect } from 'react-redux';
import { priceText } from '../../helper';
import './styles.css';

class Estate extends Component {

  componentDidMount() {
      window.scrollTo(0, 0);
  }

  convertToYesOrNo = (value) => {
    if (parseInt(value))
      return "Ir"

    return "Nav"
  }

  render() {
    const { estateData, match } = this.props;
    const data = estateData && estateData.find(item => item.id === match.params.id);

    return (
      <div className="estate-container">
        {data &&
          <>
            <Slider
              adaptiveHeight={true}
            >
              {data.images.map((item, i) => <div key={i} className="images-container"><img src={item} /></div>)}
            </Slider>
            {/* <img src={data.images[0]} /> */}
            <div className="estate-data-container">
              <div className="estate-row">
                <div className="estate-location">
                  <p className="estate-address">{data.city + ", " + data.iela + " " + data.house_no}</p>
                  <p className="estate-district">{data.district}{data.project && <span>, "{data.project}"</span>}</p>
                </div>
                <div className="estate-price-block">
                  <div className="estate-price">Cena:<span className="estate-table-second">{data.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " " + priceText(data.deal_type, false)}</span></div>
                  <div className="estate-price-per-m2">Cena m2:<span className="estate-table-second">{Math.round(data.price / data.size).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " €/m2"}</span></div>
                  <hr />
                  <div className="estate-expenses">Komunālie ziemā:<span className="estate-table-second">{data.komunalieziema ? data.komunalieziema : "-"} €</span></div>
                  <div className="estate-expenses">Komunālie vasarā:<span className="estate-table-second">{data.komunalievasara ? data.komunalievasara : "-"} €</span></div>
                  <div className="estate-expenses">Apsaimniekošana m2:<span className="estate-table-second">{data.apsaimniekosana ? data.apsaimniekosana : "-"} €</span></div>
                </div>
              </div>
              <hr />
              <div className="estate-row info">
                <div className="estate-feature-grid">
                  <p>ID:<span className="estate-table-second">{data.id}</span></p>
                  <p>Tips:<span className="estate-table-second">{data.type}</span></p>
                  <p>Darījuma veids:<span className="estate-table-second">{data.deal_type}</span></p>
                  <p>Istabas:<span className="estate-table-second">{data.rooms}</span></p>
                  <p>Platība:<span className="estate-table-second">{data.size}</span></p>
                  <p>Sērija:<span className="estate-table-second">{data.series}</span></p>
                  <p>Stāvs:<span className="estate-table-second">{data.floor + "/" + data.floor_count}</span></p>
                  <p>Lifts:<span className="estate-table-second">{this.convertToYesOrNo(data.elevator)}</span></p>
                  <p>Ērtības:<span className="estate-table-second">{data.ertibas}</span></p>
                  <p>Mājas tips:<span className="estate-table-second">{data.type_of_house}</span></p>
                  <p>Balkons:<span className="estate-table-second">{this.convertToYesOrNo(data.balcony)}</span></p>
                  <p>Terase:<span className="estate-table-second">{this.convertToYesOrNo(data.terase)}</span></p>
                </div>
                <div className="estate-contact">
                  <div className="estate-contact-image">
                    <img src={data.author_avatar} />
                  </div>
                  <div className="estate-contact-info">
                    <p className="estate-contact-author">{data.author}</p>
                    <p className="estate-contact-title">Konsultants</p>
                    <br />
                    <p>(+371) {data.author_number}</p>
                    <p><a href={`mailto:${data.author_email}`}>{data.author_email}</a></p>
                  </div>
                </div>
              </div>
              <hr />
              <div className="estate-text">
                {data.text}
              </div>
              <hr />
              {/* <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d17410.651620407876!2d24.023064899999998!3d56.94312235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2slv!4v1544381572119" width="1020" height="600" frameBorder="0" style={{border:0, marginBottom: "-50px", marginLeft: "-40px"}} allowFullScreen></iframe> */}
            </div>
          </>
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  estateData: state.data,
});

export default connect(
  mapStateToProps,
)(Estate);
