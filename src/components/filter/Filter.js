import React from 'react';
import { connect } from 'react-redux';
import { setSelectedFilterData } from '../../redux/action';
import './styles.css';

class Filter extends React.PureComponent {

  getItems = (type) => {
    const { filterData } = this.props;

    return filterData && filterData[type].map((item, i) => (
      <option key={i} value={item}>{item}</option>
    ))
  }

  onChange = (e) => {
    const { setSelectedFilterData } = this.props;

    setSelectedFilterData(e.target.name, e.target.value, e.target.id);
  }

  render() {

    return(
      <div className="filter-main-container">
        <p className="filter-title">Filtrs</p>
        <div className="filter-container">
          <div className="filter-item input">
            <label>ID: </label>
            <input onChange={this.onChange} size="4" name="id" type="number" />
          </div>
          <div className="filter-item dropdown">
            <label>Darījuma tips: </label>
            <select name='deal_type' onChange={this.onChange}>
              <option value="" defaultValue>Visi</option>
              {this.getItems('deal_type')}
            </select>
          </div>
          <div className="filter-item dropdown">
            <label>Īpašuma tips: </label>
            <select name='type' onChange={this.onChange}>
              <option value="" defaultValue>Visi</option>
              {this.getItems('type')}
            </select>
          </div>
          <div className="filter-item dropdown">
            <label>Pilsēta: </label>
            <select name='city' onChange={this.onChange}>
              <option value="" defaultValue>Visi</option>
              {this.getItems('city')}
            </select>
          </div>
          <div className="filter-item input">
            <label>Cena: </label>
            <input onChange={this.onChange} size="4" id="min" name="price" type="number" />
            <span className="seperator">:</span>
            <input onChange={this.onChange} size="4" id="max" name="price" type="number" />
          </div>
          <div className="filter-item input">
            <label>Stāvs: </label>
            <input className="small" onChange={this.onChange} size="4" id="min" name="floor" type="number" />
            <span className="seperator">:</span>
            <input className="small" onChange={this.onChange} size="4" id="max" name="floor" type="number" />
          </div>
          <div className="filter-item input">
            <label>Istabas: </label>
            <input className="small" onChange={this.onChange} size="4" id="min" name="rooms" type="number" />
            <span className="seperator">:</span>
            <input className="small" onChange={this.onChange} size="4" id="max" name="rooms" type="number" />
          </div>
          <div className="filter-item input">
            <label>Platība: </label>
            <input onChange={this.onChange} size="4" id="min" name="size" type="number" />
            <span className="seperator">:</span>
            <input onChange={this.onChange} size="4" id="mmaxn" name="size" type="number" />
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  filterData: state.filterData,
});

const mapDispatchToProps = {
  setSelectedFilterData: setSelectedFilterData,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filter);