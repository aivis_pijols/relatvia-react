import React, { Component } from 'react';
import { connect } from 'react-redux';
import Item from '../item/Item';
import Filter from '../filter/Filter';

class Catalog extends Component {
  filterItems = (data, selectedFilter) => {
    const items = data && data.filter(item => {
      for(var key in selectedFilter) {
        if(typeof selectedFilter[key] === 'object') {
          if(selectedFilter[key].min && selectedFilter[key].min > parseInt(item[key])) {
            return false
          } else {
            if(selectedFilter[key].max && selectedFilter[key].max < parseInt(item[key])) {
              return false
            }
          }
        } else {
          if(selectedFilter[key] && selectedFilter[key] !== item[key]) {
            return false
          }
        }
      }
      
      return item;
    })

    return items;
  }

  render() {
    const { data, selectedFilter } = this.props;

    return (
      <>
        <Filter />
        <div className="items-container">
          {data && this.filterItems(data, selectedFilter).map((item, i) => (
            <Item key={i} data={item} />
          ))}
        </div>
      </>
    )
  }
}

const mapStateToProps = state => ({
  data: state.data,
  selectedFilter: state.selectedFilterData
});

export default connect(
  mapStateToProps,
)(Catalog);
