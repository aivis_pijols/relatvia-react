import React, { Component } from 'react';
import men from './men.jpg';
import women from './women.jpg';
import './styles.css';

class Contacts extends Component {
  render() {
    return (
      <div className="contacts-container">
        <div className="contacts-block">
          <div className="contacts-block-title">
            <span>Par mums</span>
          </div>
          <div className="contacts-block-content">
          R|E|Latvia ir nekustamā īpašuma kompānija, kurai ir liela pieredze ar
visa veida nekustamā īpašuma pakalpojumiem, un saviem klientiem
sniedzam augstvērtīgus pakalpojumus. <br />Analizējam, izskatām visa veida
objektus, kuriem ir apgrūtinājumi un liegumi. <br />Mūsu kompānijas galvenas
mērķis ir nodrošināt kvalitātīvu pakalpojumu, lai abas puses būtu
apmierinātas un gandarītas par notikušo darījumu, kā arī nodibināt
ilgstošas augsta līmeņa attiecības. <br />Spēsim jums palīdzēt pat
vissarežģītākajā situācijā un rast izeju no tās.
<br />
<br />
Mēs strādājam nevis uz kvantitāti, bet gan uz kvalitāti, tāpēc mūsu
interesēs ir sadarbība uz ilgu laiku.<br /><br />
R|E|Latvia Jums palīdzēs :
            <ul>
              <li>Visa veida nekustamā īpašuma konsultācijas (mājas, dzīvokļi, komercplatības, zeme, meži),</li>
              <li>Nekustamā īpašuma pārdošana, pirkšana, īre un noma,</li>
              <li>Darījuma uzraudzība,</li>
              <li>Bezmaksas konsultācija,</li>
              <li>Nekustamā īpašuma piemeklēšana klientiem,</li>
              <li>Juridiskie pakalpojumi,</li>
              <li>Līguma izstrādāšana,</li>
              <li>Objekta pārvaldīšana, apsaimniekošana,</li>
              <li>Sniegsim konsultāciju, lai sagatavotu dokumentus hipotekārā kredīta noformēšanai.</li>
            </ul>
          </div>
        </div>
        <div className="contacts-block">
          <div className="contacts-block-title">
            <span>Komanda</span>
          </div>
          <div className="contacts-block-content">
            <div className="contacts-contact-container">
              <div className="contacts-contact">
                <div className="contacts-contact-image">
                  <img src={women} />
                </div>
                <div className="contacts-contact-info">
                  <p className="contacts-contact-author">Līga Ose</p>
                  <p className="contacts-contact-title">Dzīvokļu un māju īre un pārdošana</p>
                  <br />
                  <p>(+371) 26444891</p>
                  <p>liga.ose@relatvia.lv</p>
                </div>
              </div>
            </div>
            <div className="contacts-contact-container">
              <div className="contacts-contact">
                <div className="contacts-contact-image">
                  <img src={men} />
                </div>
                <div className="contacts-contact-info">
                  <p className="contacts-contact-author">Jānis Bračiška</p>
                  <p className="contacts-contact-title">Dzīvokļu un māju īre,<br /> zeme un meža īpašuma pārdošana</p>
                  <br />
                  <p>(+371) 25416880</p>
                  <p>janis.braciska@relatvia.lv</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="contacts-block">
          <div className="contacts-block-title">
            <span>Kontakti</span>
          </div>
          <div className="contacts-block-content contacts">
            <p>(+371) 26444891</p>
            <p>relatvia@relatvia.lv</p>
          </div>
        </div>
      </div>
    )
  }
}

export default Contacts;
