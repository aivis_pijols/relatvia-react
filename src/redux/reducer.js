import * as actions from './action';

const initialState = {
  loading: false,
  data: null,
  error: null,
  filterData: null,
  selectedFilterData: {
    deal_type: '',
    type: '',
    city: '',
    price: {
      min: '',
      max: ''
    },
    floor: {
      min: '',
      max: ''
    },
    size: {
      min: '',
      max: ''
    },
    rooms: {
      min: '',
      max: ''
    },
  },
};

function mainReducer(state = initialState, action) {
  switch (action.type) {
    case actions.FETCH_DATA_BEGINS:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case actions.FETCH_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
        filterData: action.payload.filterData,
      };
    case actions.FETCH_DATA_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
      };
    case actions.SET_FILTER:
      return {
        ...state,
        selectedFilterData: {
          ...state.selectedFilterData,
          ...action.payload.selectedFilterData,
          price: {
            ...state.selectedFilterData.price,
            ...action.payload.selectedFilterData.price
          },
          floor: {
            ...state.selectedFilterData.floor,
            ...action.payload.selectedFilterData.floor
          },
          size: {
            ...state.selectedFilterData.size,
            ...action.payload.selectedFilterData.size
          },
          rooms: {
            ...state.selectedFilterData.rooms,
            ...action.payload.selectedFilterData.rooms
          }
        }
      }
    default:
      return state;
  }
}

export default mainReducer;
