import axios from 'axios';

export const FETCH_DATA_BEGINS = 'FETCH_DATA_BEGINS';
export const FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS';
export const FETCH_DATA_ERROR = 'FETCH_DATA_ERROR';
export const SET_FILTER = 'SET_FILTER';

export const fetchDataBegin = () => ({
  type: FETCH_DATA_BEGINS,
});

export const fetchDataSuccess = data => {
  let filterData = {
    deal_type: [],
    type: [],
    city: []
  };
  data.forEach(item => {
    item.deal_type && !filterData.deal_type.includes(item.deal_type) && filterData.deal_type.push(item.deal_type);
    item.type && !filterData.type.includes(item.type) && filterData.type.push(item.type);
    item.city && !filterData.city.includes(item.city) && filterData.city.push(item.city);
  });
  return {
    type: FETCH_DATA_SUCCESS,
    payload: {
      data,
      filterData
    },
  }
};

export const fetchDataError = () => ({
  type: FETCH_DATA_ERROR,
});

export const setFilter = selectedFilterData => ({
  type: SET_FILTER,
  payload: {
    selectedFilterData,
  }
})

export const fetchData = () => (
  dispatch => {
    dispatch(fetchDataBegin());
    return axios.get('https://testing.relatvia.lv/api')
      .then(response => {
        if (response.status) {
          return response;
        }
        throw new Error('Something went wrong ...');
      })
      .then(response => {
        dispatch(fetchDataSuccess(response.data));
        return response.data;
      })
      .catch(() => dispatch(fetchDataError()));
  }
);

export const setSelectedFilterData = (name, value, id) => (
  dispatch => {
    let selectedData = {};

    if(id) {
      selectedData = {
        [name]: {
          [id]: value
        }
      }
    } else {
      selectedData = {
        [name]: value
      }
    }

    dispatch(setFilter(selectedData));
  }
)
