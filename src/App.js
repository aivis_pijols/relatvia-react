import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import { fetchData } from './redux/action';
import Catalog from './components/catalog/Catalog';
import Contacts from './components/contacts/Contacts';
import Estate from './components/estate/Estate';
import logo from './logo.png';
import './App.css';


class App extends Component {
  componentDidMount() {
    const { fetchDataAction } = this.props;

    fetchDataAction();
  }

  render() {
    return (
      <Router onUpdate={() => window.scrollTo(0, 0)}>
        <>
          <header>
            <div className="header-container">
              <div className="logo">
                <Link to="/">
                  <img src={logo} />
                </Link>
              </div>
              <nav>
                <ul>
                  <li><Link to="/catalog">Katalogs</Link></li>
                  <li><Link to="/contacts">Kontakti</Link></li>
                </ul>
              </nav>
            </div>
          </header>
          <main>
            <Route exact path="/" component={Catalog} />
            <Switch>
              <Route exact path="/catalog" component={Catalog} />
              <Route exact path="/catalog/:id" component={Estate} />
            </Switch>
            <Route exact path="/contacts" component={Contacts} />
          </main>
          <footer>
            <small>&copy; Copyright 2018 relatvia.lv<br /> Visas tiesības paturētas, informācijas un attēlu pārpublicēšana jāsaskaņo.</small>
          </footer>
        </>
      </Router>
    );
  }
}

const mapDispatchToProps = {
  fetchDataAction: fetchData,
};

export default connect(
  null,
  mapDispatchToProps,
)(App);
